# Router Where Art Thou? -- Web -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/web/router_where_art_thou)

## Chal Info

Desc: `Interesting login page, think you can crack it?`

Hints:

* Doesn't this page look...familiar?

Flag: `TUCTF{y0u_f0und_th3_fun_r0ut3r_d3f4ult5}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/router_where_art_thou)

Ports: 80

Example usage:

```bash
docker run -d -p 127.0.0.1:8080:80 asciioverflow/router_where_art_thou:tuctf2019
```
