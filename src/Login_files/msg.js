/*!
 * PAN Management UI
 * Copyright(c) Palo Alto Networks, Inc.
 */
umd(function(define) {
    define(function(require /*, exports, module */) {
        var Ext = require('Ext');

        var msgCt;
        function createBox(s, t) {
            return ['<div class="msg">',
                '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
                '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc">',
                //'<h3>', t, '</h3>',
                s, '</div></div></div>',
                '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
                '</div>'].join('');
        }

        var msg =  function (format) {
            var title = '';
            if (!msgCt) {
                msgCt = Ext.DomHelper.insertFirst(document.body, {id: 'pan-base-msg-div'}, true);
            }
            msgCt.alignTo(document, 't-t');
            var s = String.format.apply(String, Array.prototype.slice.call(arguments, 0));
            var m = Ext.DomHelper.append(msgCt, {html: createBox(s, title)}, true);
            m.slideIn('t').pause(2).ghost("t", {remove: true});
        };

        Ext.apply(msg, {
            error: function(msg) {
                msg = String.format.apply(null, arguments);
                Pan.Msg.show({ msg : msg, buttons : Ext.MessageBox.OK, icon : Ext.MessageBox.ERROR });
            },
            warn: function(msg) {
                msg = String.format.apply(null, arguments);
                Pan.Msg.show({ msg : msg, buttons : Ext.MessageBox.OK, icon : Ext.MessageBox.WARNING });
            },
            alert: function(msg) {
                msg = String.format.apply(null, arguments);
                Pan.Msg.show({ msg : msg, buttons : Ext.MessageBox.OK, icon : Ext.MessageBox.WARNING });
            },
            info: function() {
                var msg = String.format.apply(null, arguments);
                Pan.Msg.show({ msg : msg, buttons : Ext.MessageBox.OK, icon : Ext.MessageBox.INFO });
            }
        });

        return msg;
    });
}, "Pan.base.msg", require, exports, module);


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 * vim: sw=4 ts=4 expandtab
 */
