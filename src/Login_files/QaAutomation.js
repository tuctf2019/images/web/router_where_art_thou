/*!
 * PAN Management UI
 * Copyright(c) Palo Alto Networks, Inc.
 */
Ext.namespace('Pan.test');

Ext.Component.prototype.afterRender = Ext.Component.prototype.afterRender.createInterceptor(
        function() {
            var el = this.getEl();
            var config = this.initialConfig;
            var itemIdVal = config["itemId"];

            if (itemIdVal != undefined) {
                el.dom.setAttribute("itemId", itemIdVal);
            }
        });

/**
 * @class Pan.test.QaAutomation
 * CommentsRequred
*/
Pan.test.QaAutomation = function() {
    // return singleton object
    var all;
    return {
        all: new Array(),

        registerXPathFunction : function(cls){
            if (!Ext.isFunction(cls.prototype.getXPath)) {
                Pan.base.log(cls.xtype + " registration failed since 'getXPath' is not a function.");
                return;
            }
            this.all.push(cls);
        }

    };
}();

// private
// process the components that has a special way to handle things
Pan.test.QaAutomation.processRegisteredTypes = function(possibleNode) {
    var indx=0;
    for (indx=0; indx < this.all.length; indx++) {
        var cls = this.all[indx];
        var result = cls.prototype.getXPath(possibleNode);
        if (result != null) {
            return result;
        }
    }
    return null;
};

Pan.test.QaAutomation.getXPath = function(domId) {
    if (! Ext.isGecko) {
        Pan.base.log('This utility only works on Firefox.');
        return null;
    }
    var domObjPath = '';
    var isExtGen = (domId.indexOf('ext-gen') == 0) ;
    var isExtComp = !isExtGen && (domId.indexOf('ext-comp') == 0);
    var isExtRec = !isExtGen && !isExtComp && (domId.indexOf('ext-record') == 0);
    var isXpath = !isExtComp && (domId.indexOf('//') == 0);
    // check to see if this is an Ext generated Id or Xpath
    if (! isExtGen && ! isExtComp && ! isXpath && !isExtRec) {
        return domId; // wont do any conversion
    }
    if (isExtGen || isExtComp || isExtRec) {
        domObjPath = '//*[@id=\'' + domId + '\']';
    } else {
        domObjPath = domId; // not an id but actual path
    }
    var possibleNodes
            = document.evaluate(domObjPath, document, null, XPathResult.ANY_TYPE, null); //returns XPath result
    var possibleNode
            = possibleNodes.iterateNext(); //are there are more than one?
    if (possibleNode) {
        var pathValue = this.processRegisteredTypes(possibleNode);
        if (pathValue) {
            return pathValue;
        }

        // default handling
        pathValue = possibleNode.getAttribute("itemId");
        if (pathValue) {
            return '//*[@' + 'itemId' + '=\'' + pathValue + '\']';
        }
        return this.getXPathUpToTopLevel(possibleNode);
    }
    return domId;
};

// private
Pan.test.QaAutomation.getXPathUpToTopLevel = function(possibleNode, getRootPath) {
    if (getRootPath == undefined) {
        getRootPath = this.getRootByPathDefined;
    }
    var parentNode = possibleNode;
    var result = '';
    while (parentNode) {
        var rootPath = getRootPath(parentNode);
        if (rootPath != null) { //
            result =  rootPath + result;
            break; // done with looping
        }
        var parentNodeName = parentNode.nodeName.toLowerCase();
        var grandParentNode = parentNode.parentNode;
        var index = 0;
        var realIndex = 1;
        if (grandParentNode) {
            if (parentNodeName == "html" && grandParentNode.nodeName.toLowerCase() == "#document") {
                result = '//' + result;
                break;
            }
            for (index = 0; index < grandParentNode.childNodes.length; index++) {
                var tmpNode = grandParentNode.childNodes[index];
                if (tmpNode == parentNode) {
                    break;
                } else if (tmpNode.nodeName.toLowerCase() == parentNodeName) {
                    realIndex++;
                }
            }
        }
        if (parentNode.tagName == "DIV" && parentNode.firstChild.tagName == "TABLE") {
            result = '/'+ result;
        } else {
            if (realIndex == 1) {
                if (parentNodeName == 'em' && parentNode.firstChild.nodeType == 3) {
                    result = parentNodeName + '[text()="' + parentNode.textContent + '"]';
                } else {
                    result = parentNodeName + '/' + result;
                }
            } else {
                result = parentNodeName + '[' + realIndex + ']' + '/' + result;
            }
        }
        parentNode = parentNode.parentNode;
    }
    if (result.charAt(result.length -1) == '/') {
        result = result.substr(0, result.length -1);
    }
    return result;
};

// private, handling Extjs usual case
Pan.test.QaAutomation.getRootByPathDefined = function(parentNode) {
    if (parentNode.getAttribute) {
        var pathValue = parentNode.getAttribute('itemId');
        if (pathValue == null)
            return null;
        return '//*[@' + 'itemId' + '=\'' + pathValue + '\']/';
    }
    return  null;
};

// private, handling list, date, etc.
Pan.test.QaAutomation.getRootByDateTimeDefined = function(parentNode) {
    var indexValue = parentNode.className.indexOf('x-form-element');
    if (indexValue == -1)
        return null;
    var pathValue = parentNode.firstChild.getAttribute('itemId');
    return '//*[@' + 'itemId' + '=\'' + pathValue + '\']/../';
};

Pan.xreg = Pan.test.QaAutomation.registerXPathFunction;

/**
 * Utilities supports Selenium IDE
 */
Ext.namespace('Pan.test.QaAutoUtils');

/*
 * Disable notification if we can find any
 */
if (window.location.search.match(/AUTOMATION/)) {
    document.cookie='AUTOMATION=true; path=/';
}

if (Pan.base.cookie.get('AUTOMATION')) {
    if (Pan.appframework && Pan.appframework.PanNotificationHandler) {
        Pan.appframework.PanNotificationHandler.getInstance().__proto__.paused = true;
        Pan.appframework.PanNotificationHandler.getInstance().__proto__.disabled = true;
    }
}

Pan.test.QaAutoUtils.textStr = ",pantxt=";

/**
 * Utility used by Selenium Java API to locate objects.
 * @param locatorString
 * @return {Array}
 */
Pan.test.QaAutoUtils.findElements = function(locatorString) {
    var textIndx = locatorString.indexOf(Pan.test.QaAutoUtils.textStr);
    var text = '';
    if (textIndx > 0) {
        text = locatorString.substring(textIndx+Pan.test.QaAutoUtils.textStr.length); // escape
        locatorString = locatorString.substring(0, textIndx);
    }
    var els = Ext.query(locatorString);
    var results = [];
    if (els && els.length > 0) {
        for (var indx=0; indx < els.length; indx++) {
            if (text == '') {
                results.push(els[indx]);
            } else if (els[indx].innerHTML == text) {
                results.push(els[indx]);
            } else if (els[indx].value == text) {
                results.push(els[indx]);
            }
        }
    }
    return results;
};

/**
 * Utility used by Selenium Java API to locate object.
 * @param locatorString
 * @return {*}
 */
Pan.test.QaAutoUtils.findElement = function(locatorString) {
    var result = Pan.test.QaAutoUtils.findElements(locatorString);
    if (result && result.length > 0) {
        return result[0];
    }
    return null;
};

